#include "bmp.h"
#include <stdlib.h>

struct  __attribute__((packed)) bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static uint8_t get_padding(uint64_t wight) {
    if ((wight * 3) % 4 != 0) {
        return 4 - (wight * 3) % 4;
    }
    return 0;
}

enum read_status check_header(struct bmp_header header) {
    if (header.biBitCount != 24) { return READ_INVALID_BITS; }
    if (header.bfType != 0x4D42) { return READ_INVALID_SIGNATURE; }
    if (header.biCompression != 0) { return READ_INVALID_HEADER; }
    return READ_OK;
}

static void initialize_img(FILE *file, struct image *image, struct bmp_header header) {
    uint64_t width = header.biWidth;
    uint64_t height = header.biHeight;
    uint8_t padding = get_padding(width);

    image->width = width;
    image->height = height;
    image->data = malloc(width * height * sizeof(struct pixel));

    for (uint64_t i = 1; i <= height; i++) {
        for (uint64_t j = 1; j <= width; j++) {
            fread(&image->data[(i - 1) * width + j - 1], sizeof(struct pixel), 1, file);
        }
        fseek(file, padding, SEEK_CUR);
    }
}

enum read_status read_bmp(FILE *file, struct image *image) {
    struct bmp_header header;
    fread(&header, sizeof(struct bmp_header), 1, file);

    enum read_status status = check_header(header);
    if (status != READ_OK) return status;

    initialize_img(file, image, header);

    return READ_OK;
}

static struct bmp_header create_header(uint64_t width, uint64_t height) {
    struct bmp_header header;
    uint32_t padding = get_padding(width);
    header.bfType = 0x4D42;
    header.bfileSize = 54 + (width * 3 + padding) * height;
    header.bfReserved = 0;
    header.bOffBits = 54;
    header.biSize = 40;
    header.biWidth = width;
    header.biHeight = height;
    header.biPlanes = 1;
    header.biBitCount = 24;
    header.biCompression = 0;
    header.biSizeImage = height * (width * 3 + padding);
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;
    return header;
}

enum write_status write_bmp(FILE *file_input, struct image const *image) {
    uint64_t width = image->width;
    uint64_t height = image->height;
    uint8_t padding = get_padding(width);

    struct bmp_header header = create_header(width, height);
    fwrite(&header, sizeof(struct bmp_header), 1, file_input);

    for (uint64_t i = 1; i <= height; i++) {
        for (uint64_t j = 1; j <= width; j++) {
            fwrite(&image->data[(i - 1) * width + j - 1], sizeof(struct pixel), 1, file_input);
        }
        for (uint8_t k = 1; k <= padding; k++) {
            uint8_t zero = 0;
            fwrite(&zero, sizeof(uint8_t), 1, file_input);
        }
    }
    return WRITE_OK;
}
