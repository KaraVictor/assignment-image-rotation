#include "bmp.h"
#include "file.h"
#include "image.h"
#include "rotate.h"

#include <stdio.h>


int main(int argc, char **argv) {
    if (argc == 3) {
        FILE *file_input, *file_output = NULL;
        enum file_open_status fileOpenStatusRead = file_open_read(&file_input, argv[1]);
        enum file_open_status fileOpenStatusWrite = file_open_write(&file_output, argv[2]);
        if (fileOpenStatusRead != FILE_OPEN || fileOpenStatusWrite!= FILE_OPEN) { return 1; }

        struct image image_input;

        enum read_status readStatus = read_bmp(file_input, &image_input);
        if (readStatus != READ_OK) {
            return 1;
        }

        struct image image_rotate = rotate(image_input);

        enum write_status writeStatus = write_bmp(file_output, &image_rotate);
        if (writeStatus != WRITE_OK) {
            return 1;
        }

        delete_data_all(image_input, image_rotate);
        enum file_close_status fileCloseStatus1 = file_close(&file_input);
        enum file_close_status fileCloseStatus2 = file_close(&file_output);
        if (fileCloseStatus1 == FILE_CLOSE || fileCloseStatus2 == FILE_CLOSE) { return 0;}
    }
    return 1;
}
