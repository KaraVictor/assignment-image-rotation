#include <stdio.h>

#include "file.h"

enum file_open_status file_open_read(FILE **file_input, char *arg_reed) {
    *file_input = fopen(arg_reed, "rb");
    if (file_input == NULL) {
        return FILE_OPEN_ERROR;
    }
    return FILE_OPEN;
}
enum file_open_status file_open_write(FILE **file_output, char *arg_write) {
    *file_output = fopen(arg_write, "wb");
    if (file_output == NULL) {
        return FILE_OPEN_ERROR;
    }
    return FILE_OPEN;
}

enum file_close_status file_close(FILE **file) {
    if (fclose(*file) == 0) {
        return FILE_CLOSE;
    }
    return FILE_CLOSE_ERROR;
}
