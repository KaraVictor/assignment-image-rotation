#include "rotate.h"
#include <stdint.h>

static unsigned long get_position(struct image const source, uint64_t i, uint64_t j) {
    return j * source.height + source.height - 1 - i;
}

struct image rotate(struct image const source) {
    uint64_t width = source.width;
    uint64_t height = source.height;
    struct image image_out = {
            .width = height,
            .height = width,
            .data = malloc(height * width * sizeof(struct pixel)),
    };
    for( uint64_t i = 0; i < image_out.width; i++) {
        for( uint64_t j = 0; j < image_out.height; j++) {
            image_out.data[get_position(source, i, j)] = source.data[i * source.width + j];
        }
    }
    return image_out;
}
