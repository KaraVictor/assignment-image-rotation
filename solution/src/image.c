#include "image.h"
#include <stdint.h>
#include <stdlib.h>

void delete_data_all(struct image image_input, struct image image_output) {
    free(image_input.data);
    free(image_output.data);
}
