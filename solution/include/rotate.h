#ifndef ASSIGNMENT_IMAGE_ROTATION_ROTATE_90_H
#define ASSIGNMENT_IMAGE_ROTATION_ROTATE_90_H
#include "image.h"

struct image rotate(struct image const source);

#endif //ASSIGNMENT_IMAGE_ROTATION_ROTATE_90_H
