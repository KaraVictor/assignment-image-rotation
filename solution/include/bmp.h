#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_H

#include "image.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
};

enum read_status read_bmp(FILE *file, struct image *image);

enum  write_status  {
    WRITE_OK = 0
};

enum write_status write_bmp(FILE *file_input, struct image const *image);

#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_H
