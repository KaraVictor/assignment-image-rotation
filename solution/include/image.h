#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

struct pixel { uint8_t b, g, r; };

struct image {
    uint64_t width, height;
    struct pixel *data;
};

void delete_data_all(struct image image_input, struct image image_output);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
