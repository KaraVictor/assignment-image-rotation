#ifndef IMAGE_TRANSFORMER_FILE_H
#define IMAGE_TRANSFORMER_FILE_H

#include <stdio.h>

enum file_open_status {
    FILE_OPEN,
    FILE_OPEN_ERROR
};

enum file_close_status {
    FILE_CLOSE,
    FILE_CLOSE_ERROR
};

enum file_open_status file_open_read(FILE **file_input, char *arg_reed);
enum file_open_status file_open_write(FILE **file_output, char *arg_write);

enum file_close_status file_close(FILE **file);

#endif //IMAGE_TRANSFORMER_FILE_H
